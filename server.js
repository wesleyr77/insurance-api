var express = require('express');
var app = express();
var bodyParser = require('body-parser');


var carData = require('./data/car-data.json');

 app.use(bodyParser.urlencoded({
    	extended: true
 }));

 app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.send('Hello World! ' + JSON.stringify(carData));
});


app.post('/car-details', function(req, res){


	console.log('request data ', req.body);

	findCarData(req, res, req.body.car);
});




app.get('/car-details/:carreg', function(req, res){

	console.log('car reg in the get :: ' + req.params.carreg);

	console.log('request data ', req.params);

	findCarData(req, res, req.params.carreg);
});



app.listen(4000, function () {
  console.log('Example app listening on port 4000!');
});




//------------------------------------------------------------------------------

function findCarData(req, res, carReg) {
	for (var car in carData.cars){
		if (carData.cars[car].reg === carReg) {
			return res.json(carData.cars[car]);
		}

	}


	res.json({data : "this is the car " + carReg});

}